package com.geoquiz.model.quiz;

/**
 * This eneum represents the difficulties available for the categories Capitali and Monumenti.
 *
 */
public enum BasicMode implements Mode {
    /**
     * The challenge level. You have only one life.
     */
    CHALLENGE,
    /**
     * The training mode. You have no lives.
     */
    TRAINING

}
