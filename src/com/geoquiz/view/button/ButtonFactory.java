package com.geoquiz.view.button;

import javafx.scene.paint.Color;

/**
 * static factory for buttons inside menu.
 */
public final class ButtonFactory {

    private ButtonFactory() {
    }

    /**
     * 
     * @param name
     *            the text of button.
     * 
     * @param colorBackground
     *            the color of button background.
     * 
     * @param width
     *            the width of button.
     * 
     * @return the button.
     */
    public static GameButton createButton(final String name, final Color colorBackground, final double width) {
        return new GameButtonImpl(name, colorBackground, width);
    }

}
