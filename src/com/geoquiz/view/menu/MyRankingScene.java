package com.geoquiz.view.menu;

import java.io.IOException;
import java.util.Map;

import com.geoquiz.utility.Pair;
import com.geoquiz.view.button.GameButtonsCategory;

import javafx.stage.Stage;

/**
 * The statistics scene where user can see own records.
 */
public class MyRankingScene extends AbsoluteRankingScene {

    private Map<Pair<String, String>, Integer> map;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public MyRankingScene(final Stage mainStage){
        super(mainStage);
        super.getTitle().setText("My records");
        try {
            map = super.getRanking().getPersonalRanking(LoginMenuScene.getUsername());
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        super.clearLabels();

        super.getCapitalsEasy().setText(super.getCapitalsEasy().getText()
                + this.getRecordByCategory(GameButtonsCategory.CAPITALI.toString(), GameButtonsCategory.FACILE.toString()));
        super.getCapitalsMedium().setText(super.getCapitalsMedium().getText()
                + this.getRecordByCategory(GameButtonsCategory.CAPITALI.toString(), GameButtonsCategory.MEDIO.toString()));
        super.getCapitalsHard().setText(super.getCapitalsHard().getText()
                + this.getRecordByCategory(GameButtonsCategory.CAPITALI.toString(), GameButtonsCategory.DIFFICILE.toString()));
        super.getCapitalsChallenge().setText(super.getCapitalsChallenge().getText()
                + this.getRecordByCategory(GameButtonsCategory.CAPITALI.toString(), GameButtonsCategory.SFIDA.toString()));
        super.getMonumentsEasy().setText(super.getMonumentsEasy().getText()
                + this.getRecordByCategory(GameButtonsCategory.MONUMENTI.toString(), GameButtonsCategory.FACILE.toString()));
        super.getMonumentsMedium().setText(super.getMonumentsMedium().getText()
                + this.getRecordByCategory(GameButtonsCategory.MONUMENTI.toString(), GameButtonsCategory.MEDIO.toString()));
        super.getMonumentsHard().setText(super.getMonumentsHard().getText()
                + this.getRecordByCategory(GameButtonsCategory.MONUMENTI.toString(), GameButtonsCategory.DIFFICILE.toString()));
        super.getMonumentsChallenge().setText(super.getMonumentsChallenge().getText()
                + this.getRecordByCategory(GameButtonsCategory.MONUMENTI.toString(), GameButtonsCategory.SFIDA.toString()));
        super.getFlagsClassic().setText(super.getFlagsClassic().getText()
                + this.getRecordByCategory(GameButtonsCategory.BANDIERE.toString(), GameButtonsCategory.CLASSICA.toString()));
        super.getFlagsChallenge().setText(super.getFlagsChallenge().getText()
                + this.getRecordByCategory(GameButtonsCategory.BANDIERE.toString(), GameButtonsCategory.SFIDA.toString()));
        super.getCurrenciesClassic().setText(super.getCurrenciesClassic().getText()
                + this.getRecordByCategory(GameButtonsCategory.VALUTE.toString(), GameButtonsCategory.CLASSICA.toString()));
        super.getCurrenciesChallenge().setText(super.getCurrenciesChallenge().getText()
                + this.getRecordByCategory(GameButtonsCategory.VALUTE.toString(), GameButtonsCategory.SFIDA.toString()));
        super.getDishesClassic().setText(super.getDishesClassic().getText()
                + this.getRecordByCategory(GameButtonsCategory.CUCINA.toString(), GameButtonsCategory.CLASSICA.toString()));
        super.getDishesChallenge().setText(super.getDishesChallenge().getText()
                + this.getRecordByCategory(GameButtonsCategory.CUCINA.toString(), GameButtonsCategory.SFIDA.toString()));

    }

    private String getRecordByCategory(final String category, final String difficulty) {
        final Integer record = this.map.get(new Pair<>(category, difficulty));
        return record == null ? "" : record.toString();
    }
}