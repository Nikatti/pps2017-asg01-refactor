package com.geoquiz.view.menu;

import com.geoquiz.view.button.GameButton;
import com.geoquiz.view.button.ButtonFactory;
import com.geoquiz.view.button.GameButtonsCategory;
import com.geoquiz.view.label.GameLabel;
import com.geoquiz.view.utility.Background;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import com.geoquiz.view.button.GameButtons;
import com.geoquiz.view.label.GameLabelFactory;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The scene where user can choose game modality.
 */
public class ModeScene extends Scene {

    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 350;
    private static final double POS_3_Y = 200;
    private static final double POS_Y_BACK = 600;
    private static final double POS_1_X = 100;
    private static final double LABEL_FONT = 35;
    private static final double BUTTON_WIDTH = 350;
    private static final double OPACITY = 0.5;
    private static final double USER_LABEL_FONT = 40;

    private final Pane panel = new Pane();
    private final VBox vbox = new VBox();
    private final VBox vbox2 = new VBox();
    private final VBox vbox3 = new VBox();
    private static final Text BUTTONPRESSED = new Text();
    private final javafx.scene.control.Label label = new javafx.scene.control.Label();

    private GameButtonsCategory categoryPressed;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public ModeScene(GameButtonsCategory categoryPressed, final Stage mainStage) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final GameButton back;
        final GameButton classic;
        final GameButton challenge;
        final GameButton training;

        this.categoryPressed = categoryPressed;

        final GameLabel userGameLabel = GameLabelFactory.createMyLabel("USER: " + LoginMenuScene.getUsername(), Color.BLACK,
                USER_LABEL_FONT);

        back = ButtonFactory.createButton(GameButtons.INDIETRO.toString(), Color.BLUE, BUTTON_WIDTH);
        classic = ButtonFactory.createButton(GameButtonsCategory.CLASSICA.toString(), Color.BLUE, BUTTON_WIDTH);
        challenge = ButtonFactory.createButton(GameButtonsCategory.SFIDA.toString(), Color.BLUE, BUTTON_WIDTH);
        training = ButtonFactory.createButton(GameButtonsCategory.ALLENAMENTO.toString(), Color.BLUE, BUTTON_WIDTH);

        ImageView image;

        if (this.categoryPressed == GameButtonsCategory.CAPITALI) {
            label.setText("Sai indicare la capitale di ciascun paese?\nScegli prima la modalità di gioco!");
            image = Background.getCategoryImage("/images/capitali.jpg");
        } else if (this.categoryPressed == GameButtonsCategory.MONUMENTI) {
            label.setText("Sai indicacare dove si trovano questi famosi monumenti?\nScegli prima la modalità di gioco!");
            image = Background.getCategoryImage("/images/monumenti.jpg");
        } else if (this.categoryPressed == GameButtonsCategory.BANDIERE){
            label.setText("Sai indicare i paesi in base alla bandiera nazionale?\nScegli prima la modalità di gioco!");
            image = Background.getCategoryImage("/images/bandiere.jpg");
        } else if (this.categoryPressed.equals(GameButtonsCategory.CUCINA.toString())) {
            label.setText("Sai indicare di quali paesi sono tipici questi piatti?\nScegli prima la modalità di gioco!");
            image = Background.getCategoryImage("/images/cucina.jpg");
        } else {
            label.setText("Sai indicare qual e' la valuta adottata da ciascun paese?\nScegli prima la modalità di gioco!");
            image = Background.getCategoryImage("/images/valute.jpg");

        }

        label.setFont(Font.font("Italic", FontWeight.BOLD, LABEL_FONT));

        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_2_Y);
        vbox.getChildren().addAll((Node) classic, (Node) challenge, (Node) training);
        vbox2.getChildren().add((Node) back);
        vbox3.getChildren().add(label);

        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS_Y_BACK);
        vbox3.setTranslateX(POS_2_X);
        vbox3.setTranslateY(POS_3_Y);

        ((Node) back).setOnMouseClicked(event -> {
            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            mainStage.setScene(new CategoryScene(mainStage));
        });

        ((Node) classic).setOnMouseClicked(event -> {
            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            ModeScene.BUTTONPRESSED.setText(GameButtonsCategory.CLASSICA.toString());
            if (this.categoryPressed == GameButtonsCategory.CAPITALI || this.categoryPressed == GameButtonsCategory.MONUMENTI) {
                mainStage.setScene(new LevelScene(mainStage));
            } else {
                try {
                    try {
                        mainStage.setScene(new QuizGamePlay(mainStage));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
        });

        ((Node) challenge).setOnMouseClicked(event -> {
            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            ModeScene.BUTTONPRESSED.setText(GameButtonsCategory.SFIDA.toString());
            try {
                try {
                    mainStage.setScene(new QuizGamePlay(mainStage));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        });

        ((Node) training).setOnMouseClicked(event -> {
            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            ModeScene.BUTTONPRESSED.setText(GameButtonsCategory.ALLENAMENTO.toString());
            try {
                try {
                    mainStage.setScene(new QuizGamePlay(mainStage));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        });

        image.setOpacity(OPACITY);
        this.panel.getChildren().addAll(image, Background.createBackground(),
                Background.getLogo(), vbox, vbox2, vbox3, (Node) userGameLabel);

        this.setRoot(this.panel);

    }

    /**
     * @return game modality.
     */
    public static String getModalityPressed() {
        return BUTTONPRESSED.getText();
    }
}
