package com.geoquiz.view.menu;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import com.geoquiz.controller.account.Account;
import com.geoquiz.controller.account.AccountImpl;
import com.geoquiz.view.button.GameButton;
import com.geoquiz.view.button.ButtonFactory;
import com.geoquiz.view.button.GameButtons;
import com.geoquiz.view.label.GameLabel;
import com.geoquiz.view.label.GameLabelFactory;
import com.geoquiz.view.utility.Background;
import com.geoquiz.view.utility.ExitProgram;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * The scene where user can choose how to do.
 */
public class MainMenuScene extends Scene {

    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 450;
    private static final double BUTTON_WIDTH = 350;
    private static final double USER_LABEL_FONT = 40;
    private static final double POS_X_INSTRUCTIONS = 900;
    private static final double POS_Y_INSTRUCTIONS = 638;

    private final Pane panel = new Pane();
    private final VBox vbox = new VBox();
    private final VBox instructionsButtonBox = new VBox();

    /**
     * @param mainStage
     *            the stage where the scene is called.
     * @throws IOException
     *             for exception.
     */
    public MainMenuScene(final Stage mainStage) throws IOException {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final GameButton play;
        final GameButton ranking;
        final GameButton options;
        final GameButton exit;
        final GameButton stats;
        final GameButton instructions;
        final Account a = new AccountImpl("account.txt");

        final GameLabel userGameLabel = GameLabelFactory.createMyLabel("USER: " + LoginMenuScene.getUsername(), Color.BLACK,
                USER_LABEL_FONT);

        play = ButtonFactory.createButton(GameButtons.GIOCA.toString(), Color.BLUE, BUTTON_WIDTH);
        ranking = ButtonFactory.createButton(GameButtons.CLASSIFICA.toString(), Color.BLUE, BUTTON_WIDTH);
        options = ButtonFactory.createButton(GameButtons.OPZIONI.toString(), Color.BLUE, BUTTON_WIDTH);
        exit = ButtonFactory.createButton(GameButtons.ESCI.toString(), Color.BLUE, BUTTON_WIDTH);
        stats = ButtonFactory.createButton(GameButtons.STATISTICHE.toString(), Color.BLUE, BUTTON_WIDTH);
        instructions = ButtonFactory.createButton(GameButtons.ISTRUZIONI.toString(), Color.BLUE, BUTTON_WIDTH);

        instructionsButtonBox.setTranslateX(POS_X_INSTRUCTIONS);
        instructionsButtonBox.setTranslateY(POS_Y_INSTRUCTIONS);
        instructionsButtonBox.getChildren().add((Node) instructions);

        vbox.setTranslateX(POS_1_X);
        vbox.setTranslateY(POS_1_Y);
        vbox.getChildren().addAll((Node) play, (Node) stats, (Node) ranking, (Node) options, (Node) exit);

        ((Node) exit).setOnMouseClicked(event -> {
            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            a.logout();
            ExitProgram.exitProgram(mainStage);
        });

        ((Node) instructions).setOnMouseClicked(event -> setNewScene("Instruction",mainStage));

        ((Node) stats).setOnMouseClicked(event -> setNewScene("Ranking",mainStage));

        ((Node) ranking).setOnMouseClicked(event -> setNewScene("AbsoluteRanking",mainStage));

        ((Node) options).setOnMouseClicked(event -> setNewScene("Option",mainStage));

        ((Node) play).setOnMouseClicked(event -> setNewScene("Category",mainStage));

        this.panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox,
                Background.getLogo(), (Node) userGameLabel, instructionsButtonBox);

        this.setRoot(this.panel);
    }

    private static void setNewScene(String scene,Stage mainStage) {
        if (!MainWindow.isWavDisabled()) {
            MainWindow.playClick();
        }
        switch (scene) {
            case "Instruction":
                mainStage.setScene(new InstructionScene(mainStage));
                break;
            case "Ranking":
                mainStage.setScene(new MyRankingScene(mainStage));
                break;
            case "AbsoluteRanking":
                mainStage.setScene(new AbsoluteRankingScene(mainStage));
                break;
            case "Option":
                mainStage.setScene(new OptionScene(mainStage));
                break;
            case "Category":
                mainStage.setScene(new CategoryScene(mainStage));
                break;


        }
    }

}
