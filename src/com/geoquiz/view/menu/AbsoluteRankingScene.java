package com.geoquiz.view.menu;

import java.io.IOException;
import java.util.Map;

import com.geoquiz.controller.ranking.Ranking;
import com.geoquiz.utility.Pair;
import com.geoquiz.view.button.ButtonFactory;
import com.geoquiz.view.button.GameButtons;
import com.geoquiz.view.button.GameButtonsCategory;
import com.geoquiz.view.button.GameButton;
import com.geoquiz.view.label.GameLabel;
import com.geoquiz.view.label.GameLabelFactory;
import com.geoquiz.view.utility.Background;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * The ranking scene where user can see other user's records.
 */
public class AbsoluteRankingScene extends Scene {

    private static final double POS_1_X = 20;
    private static final double POS_Y_BACK = 650;
    private static final double FONT = 35;
    private static final double BUTTON_WIDTH = 350;
    private static final double FONT_MODE = 25;
    private static final double POS_X_CATEGORY_BOX = 50;
    private static final double POS_Y_CATEGORY_BOX = 200;
    private static final double POS_X_CATEGORY_BOX_2 = 650;
    private static final double POS_Y_CATEGORY_BOX_2 = 75;
    private static final double POS_X_CAPITALS_BOX = 300;
    private static final double POS_Y_CAPITALS_BOX = 205;
    private static final double POS_X_MONUMENTS_BOX = 300;
    private static final double POS_Y_MONUMENTS_BOX = 450;
    private static final double POS_X_FLAGS_BOX = 850;
    private static final double POS_Y_FLAGS_BOX = 80;
    private static final double POS_X_CURRENCIES_BOX = 850;
    private static final double POS_Y_CURRENCIES_BOX = 325;
    private static final double POS_X_DISHES_BOX = 850;
    private static final double POS_Y_DISHES_BOX = 570;
    private static final double TITLE_FONT = 95;
    private static final String S = " -> ";

    private final Pane panel = new Pane();
    private final VBox vbox = new VBox();
    private final VBox categoryBox = new VBox(200);
    private final VBox categoryBox2 = new VBox(200);
    private final VBox capitalsBox = new VBox();
    private final VBox monumentsBox = new VBox();
    private final VBox dishesBox = new VBox();
    private final VBox currenciesBox = new VBox();
    private final VBox flagsBox = new VBox();
    private final VBox titleBox = new VBox();

    private final GameLabel title;

    private final GameLabel capitalsEasy;
    private final GameLabel capitalsMedium;
    private final GameLabel capitalsHard;
    private final GameLabel capitalsChallenge;
    private final GameLabel monumentsEasy;
    private final GameLabel monumentsMedium;
    private final GameLabel monumentsHard;
    private final GameLabel monumentsChallenge;
    private final GameLabel flagsClassic;
    private final GameLabel flagsChallenge;
    private final GameLabel currenciesClassic;
    private final GameLabel currenciesChallenge;
    private final GameLabel dishesClassic;
    private final GameLabel dishesChallenge;

    private final Ranking ranking = Ranking.getInstance();

    private Map<Pair<String, String>, Pair<String, Integer>> map;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     *
     */
    public AbsoluteRankingScene(final Stage mainStage){
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final GameLabel capitals;
        final GameLabel monuments;
        final GameLabel flags;
        final GameLabel currencies;
        final GameLabel dishes;

        final GameButton back;

        title = GameLabelFactory.createMyLabel("Global records", Color.BLACK, TITLE_FONT);
        try {
            map = this.ranking.getGlobalRanking();
        } catch (ClassNotFoundException | IOException e1) {
            e1.printStackTrace();
        }

        back = ButtonFactory.createButton(GameButtons.INDIETRO.toString(), Color.BLUE, BUTTON_WIDTH);

        capitals = GameLabelFactory.createMyLabel(GameButtonsCategory.CAPITALI.toString(), Color.RED, FONT);
        monuments = GameLabelFactory.createMyLabel(GameButtonsCategory.MONUMENTI.toString(), Color.RED, FONT);
        flags = GameLabelFactory.createMyLabel(GameButtonsCategory.BANDIERE.toString(), Color.RED, FONT);
        currencies = GameLabelFactory.createMyLabel(GameButtonsCategory.VALUTE.toString(), Color.RED, FONT);
        dishes = GameLabelFactory.createMyLabel(GameButtonsCategory.CUCINA.toString(), Color.RED, FONT);

        capitalsEasy = GameLabelFactory.createMyLabel(GameButtonsCategory.FACILE.toString() + S
                + this.getRecordbyCategory(GameButtonsCategory.CAPITALI.toString(), GameButtonsCategory.FACILE.toString()),
                Color.BLACK, FONT_MODE);
        capitalsMedium = GameLabelFactory.createMyLabel(GameButtonsCategory.MEDIO.toString() + S
                + this.getRecordbyCategory(GameButtonsCategory.CAPITALI.toString(), GameButtonsCategory.MEDIO.toString()),
                Color.BLACK, FONT_MODE);
        capitalsHard = GameLabelFactory.createMyLabel(GameButtonsCategory.DIFFICILE.toString() + S
                + this.getRecordbyCategory(GameButtonsCategory.CAPITALI.toString(), GameButtonsCategory.DIFFICILE.toString()),
                Color.BLACK, FONT_MODE);
        capitalsChallenge = GameLabelFactory.createMyLabel(GameButtonsCategory.SFIDA.toString() + S
                + this.getRecordbyCategory(GameButtonsCategory.CAPITALI.toString(), GameButtonsCategory.SFIDA.toString()),
                Color.BLACK, FONT_MODE);
        monumentsEasy = GameLabelFactory.createMyLabel(GameButtonsCategory.FACILE.toString() + S
                + this.getRecordbyCategory(GameButtonsCategory.MONUMENTI.toString(), GameButtonsCategory.FACILE.toString()),
                Color.BLACK, FONT_MODE);
        monumentsMedium = GameLabelFactory.createMyLabel(GameButtonsCategory.MEDIO.toString() + S
                + this.getRecordbyCategory(GameButtonsCategory.MONUMENTI.toString(), GameButtonsCategory.MEDIO.toString()),
                Color.BLACK, FONT_MODE);
        monumentsHard = GameLabelFactory.createMyLabel(
                GameButtonsCategory.DIFFICILE.toString() + S + this.getRecordbyCategory(
                        GameButtonsCategory.MONUMENTI.toString(), GameButtonsCategory.DIFFICILE.toString()),
                Color.BLACK, FONT_MODE);
        monumentsChallenge = GameLabelFactory.createMyLabel(GameButtonsCategory.SFIDA.toString() + S
                + this.getRecordbyCategory(GameButtonsCategory.MONUMENTI.toString(), GameButtonsCategory.SFIDA.toString()),
                Color.BLACK, FONT_MODE);
        flagsClassic = GameLabelFactory.createMyLabel(GameButtonsCategory.CLASSICA.toString() + S
                + this.getRecordbyCategory(GameButtonsCategory.BANDIERE.toString(), GameButtonsCategory.CLASSICA.toString()),
                Color.BLACK, FONT_MODE);
        flagsChallenge = GameLabelFactory.createMyLabel(GameButtonsCategory.SFIDA.toString() + S
                + this.getRecordbyCategory(GameButtonsCategory.BANDIERE.toString(), GameButtonsCategory.SFIDA.toString()),
                Color.BLACK, FONT_MODE);
        currenciesClassic = GameLabelFactory.createMyLabel(GameButtonsCategory.CLASSICA.toString() + S
                + this.getRecordbyCategory(GameButtonsCategory.VALUTE.toString(), GameButtonsCategory.CLASSICA.toString()),
                Color.BLACK, FONT_MODE);
        currenciesChallenge = GameLabelFactory.createMyLabel(
                GameButtonsCategory.SFIDA.toString() + S
                        + this.getRecordbyCategory(GameButtonsCategory.VALUTE.toString(), GameButtonsCategory.SFIDA.toString()),
                Color.BLACK, FONT_MODE);
        dishesClassic = GameLabelFactory.createMyLabel(GameButtonsCategory.CLASSICA.toString() + S
                + this.getRecordbyCategory(GameButtonsCategory.CUCINA.toString(), GameButtonsCategory.CLASSICA.toString()),
                Color.BLACK, FONT_MODE);
        dishesChallenge = GameLabelFactory.createMyLabel(
                GameButtonsCategory.SFIDA.toString() + S
                        + this.getRecordbyCategory(GameButtonsCategory.CUCINA.toString(), GameButtonsCategory.SFIDA.toString()),
                Color.BLACK, FONT_MODE);

        titleBox.getChildren().add((Node) title);

        capitalsBox.getChildren().addAll((Node) capitalsEasy, (Node) capitalsMedium, (Node) capitalsHard,
                (Node) capitalsChallenge);
        monumentsBox.getChildren().addAll((Node) monumentsEasy, (Node) monumentsMedium, (Node) monumentsHard,
                (Node) monumentsChallenge);
        currenciesBox.getChildren().addAll((Node) currenciesClassic, (Node) currenciesChallenge);
        flagsBox.getChildren().addAll((Node) flagsClassic, (Node) flagsChallenge);
        dishesBox.getChildren().addAll((Node) dishesClassic, (Node) dishesChallenge);

        categoryBox.getChildren().addAll((Node) capitals, (Node) monuments);
        categoryBox2.getChildren().addAll((Node) flags, (Node) currencies, (Node) dishes);

        categoryBox.setTranslateX(POS_X_CATEGORY_BOX);
        categoryBox.setTranslateY(POS_Y_CATEGORY_BOX);
        categoryBox2.setTranslateX(POS_X_CATEGORY_BOX_2);
        categoryBox2.setTranslateY(POS_Y_CATEGORY_BOX_2);
        capitalsBox.setTranslateX(POS_X_CAPITALS_BOX);
        capitalsBox.setTranslateY(POS_Y_CAPITALS_BOX);
        monumentsBox.setTranslateX(POS_X_MONUMENTS_BOX);
        monumentsBox.setTranslateY(POS_Y_MONUMENTS_BOX);
        dishesBox.setTranslateX(POS_X_DISHES_BOX);
        dishesBox.setTranslateY(POS_Y_DISHES_BOX);
        flagsBox.setTranslateX(POS_X_FLAGS_BOX);
        flagsBox.setTranslateY(POS_Y_FLAGS_BOX);
        currenciesBox.setTranslateX(POS_X_CURRENCIES_BOX);
        currenciesBox.setTranslateY(POS_Y_CURRENCIES_BOX);

        vbox.setTranslateX(POS_1_X);
        vbox.setTranslateY(POS_Y_BACK);
        vbox.getChildren().add((Node) back);

        ((Node) back).setOnMouseClicked(event -> {
            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        this.panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox, categoryBox,
                categoryBox2, capitalsBox, monumentsBox, flagsBox, currenciesBox, dishesBox, titleBox);

        this.setRoot(this.panel);
    }

    private String getRecordbyCategory(final String category, final String difficulty) {
        final Pair<String, Integer> record = this.map.get(new Pair<>(category, difficulty));
        return record == null ? "" : record.getX() + " " + "(" + record.getY() + ")";
    }

    /**
     * clear records labels.
     */
    protected void clearLabels() {

        capitalsEasy.setText(capitalsEasy.getText().substring(0,
                capitalsEasy.getText().indexOf(" ", capitalsEasy.getText().indexOf(" ") + 1) + 1));
        capitalsMedium.setText(capitalsMedium.getText().substring(0,
                capitalsMedium.getText().indexOf(" ", capitalsMedium.getText().indexOf(" ") + 1) + 1));
        capitalsHard.setText(capitalsHard.getText().substring(0,
                capitalsHard.getText().indexOf(" ", capitalsHard.getText().indexOf(" ") + 1) + 1));
        capitalsChallenge.setText(capitalsChallenge.getText().substring(0,
                capitalsChallenge.getText().indexOf(" ", capitalsChallenge.getText().indexOf(" ") + 1) + 1));
        monumentsEasy.setText(capitalsEasy.getText().substring(0,
                monumentsEasy.getText().indexOf(" ", monumentsEasy.getText().indexOf(" ") + 1) + 1));
        monumentsMedium.setText(monumentsMedium.getText().substring(0,
                monumentsMedium.getText().indexOf(" ", monumentsMedium.getText().indexOf(" ") + 1) + 1));
        monumentsHard.setText(monumentsHard.getText().substring(0,
                monumentsHard.getText().indexOf(" ", monumentsHard.getText().indexOf(" ") + 1) + 1));
        monumentsChallenge.setText(monumentsChallenge.getText().substring(0,
                monumentsChallenge.getText().indexOf(" ", monumentsChallenge.getText().indexOf(" ") + 1) + 1));
        flagsClassic.setText(flagsClassic.getText().substring(0,
                flagsClassic.getText().indexOf(" ", flagsClassic.getText().indexOf(" ") + 1) + 1));
        flagsChallenge.setText(flagsChallenge.getText().substring(0,
                flagsChallenge.getText().indexOf(" ", flagsChallenge.getText().indexOf(" ") + 1) + 1));
        currenciesClassic.setText(currenciesClassic.getText().substring(0,
                currenciesClassic.getText().indexOf(" ", currenciesClassic.getText().indexOf(" ") + 1) + 1));
        currenciesChallenge.setText(currenciesChallenge.getText().substring(0,
                currenciesChallenge.getText().indexOf(" ", currenciesChallenge.getText().indexOf(" ") + 1) + 1));
        dishesClassic.setText(dishesClassic.getText().substring(0,
                dishesClassic.getText().indexOf(" ", dishesClassic.getText().indexOf(" ") + 1) + 1));
        dishesChallenge.setText(dishesChallenge.getText().substring(0,
                dishesChallenge.getText().indexOf(" ", dishesChallenge.getText().indexOf(" ") + 1) + 1));
    }

    /**
     * gets the value of title label.
     * 
     * @return title label.
     */
    protected GameLabel getTitle() {
        return title;
    }

    /**
     * gets the value of facilecap label.
     * 
     * @return facilecap label.
     */
    protected GameLabel getCapitalsEasy() {
        return capitalsEasy;
    }

    /**
     * gets the value of mediocap label.
     * 
     * @return mediocap label.
     */
    protected GameLabel getCapitalsMedium() {
        return capitalsMedium;
    }

    /**
     * gets the value of difficilecap label.
     * 
     * @return difficilecap label.
     */
    protected GameLabel getCapitalsHard() {
        return capitalsHard;
    }

    /**
     * gets the value of sfidacap label.
     * 
     * @return sfidacap label.
     */
    protected GameLabel getCapitalsChallenge() {
        return capitalsChallenge;
    }

    /**
     * gets the value of facilemon label.
     * 
     * @return facilemon label.
     */
    protected GameLabel getMonumentsEasy() {
        return monumentsEasy;
    }

    /**
     * gets the value of mediomon label.
     * 
     * @return mediomon label.
     */
    protected GameLabel getMonumentsMedium() {
        return monumentsMedium;
    }

    /**
     * gets the value of difficilemon label.
     * 
     * @return difficilemon label.
     */
    protected GameLabel getMonumentsHard() {
        return monumentsHard;
    }

    /**
     * gets the value of sfidamon label.
     * 
     * @return sfidamon label.
     */
    protected GameLabel getMonumentsChallenge() {
        return monumentsChallenge;
    }

    /**
     * gets the value of classicaban label.
     * 
     * @return classicaban label.
     */
    protected GameLabel getFlagsClassic() {
        return flagsClassic;
    }

    /**
     * gets the value of sfidaban label.
     * 
     * @return sfidaban label.
     */
    protected GameLabel getFlagsChallenge() {
        return flagsChallenge;
    }

    /**
     * gets the value of classicaval label.
     * 
     * @return classicaval.
     */
    protected GameLabel getCurrenciesClassic() {
        return currenciesClassic;
    }

    /**
     * gets the value of sfidaval label.
     * 
     * @return sfidaval label.
     */
    protected GameLabel getCurrenciesChallenge() {
        return currenciesChallenge;
    }

    /**
     * gets the value of classicacuc label.
     * 
     * @return classicacuc label.
     */
    protected GameLabel getDishesClassic() {
        return dishesClassic;
    }

    /**
     * gets the value of sfidacuc label.
     * 
     * @return sfidacuc label.
     */
    protected GameLabel getDishesChallenge() {
        return dishesChallenge;
    }

    /**
     * gets the controller.
     * 
     * @return controller.
     */
    protected Ranking getRanking() {
        return this.ranking;
    }
}
