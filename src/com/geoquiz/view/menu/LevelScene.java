package com.geoquiz.view.menu;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import com.geoquiz.view.button.GameButton;
import com.geoquiz.view.button.ButtonFactory;
import com.geoquiz.view.button.GameButtons;
import com.geoquiz.view.button.GameButtonsCategory;
import com.geoquiz.view.label.GameLabel;
import com.geoquiz.view.label.GameLabelFactory;
import com.geoquiz.view.utility.Background;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The scene where user can choose difficulty level.
 */
public class LevelScene extends Scene {

    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_Y_BACK = 600;
    private static final double POS_1_X = 100;
    private static final double BUTTON_WIDTH = 350;
    private static final double USER_LABEL_FONT = 40;

    private final Pane panel = new Pane();
    private final VBox vbox = new VBox();
    private final VBox vbox2 = new VBox();
    private static final Text BUTTONPRESSED = new Text();

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public LevelScene(final Stage mainStage) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final GameLabel userGameLabel = GameLabelFactory.createMyLabel("USER: " + LoginMenuScene.getUsername(), Color.BLACK,
                USER_LABEL_FONT);

        final GameButton back;
        final GameButton easy;
        final GameButton medium;
        final GameButton hard;

        back = ButtonFactory.createButton(GameButtons.INDIETRO.toString(), Color.BLUE, BUTTON_WIDTH);
        easy = ButtonFactory.createButton(GameButtonsCategory.FACILE.toString(), Color.BLUE, BUTTON_WIDTH);
        medium = ButtonFactory.createButton(GameButtonsCategory.MEDIO.toString(), Color.BLUE, BUTTON_WIDTH);
        hard = ButtonFactory.createButton(GameButtonsCategory.DIFFICILE.toString(), Color.BLUE, BUTTON_WIDTH);

        vbox.getChildren().addAll((Node) easy, (Node) medium, (Node) hard);
        vbox2.getChildren().add((Node) back);

        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_2_Y);

        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS_Y_BACK);

        ((Node) back).setOnMouseClicked(event -> {
            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            mainStage.setScene(new CategoryScene(mainStage));
        });

        ((Node) easy).setOnMouseClicked(event -> setButtonHandler(GameButtonsCategory.FACILE,mainStage));

        ((Node) medium).setOnMouseClicked(event -> setButtonHandler(GameButtonsCategory.MEDIO,mainStage));

        ((Node) hard).setOnMouseClicked(event -> setButtonHandler(GameButtonsCategory.DIFFICILE,mainStage));

        this.panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox, vbox2,
                Background.getLogo(), (Node) userGameLabel);

        this.setRoot(this.panel);

    }

    private static void setButtonHandler(GameButtonsCategory buttonCategory, Stage mainStage){
        if (!MainWindow.isWavDisabled()) {
            MainWindow.playClick();
        }
        LevelScene.BUTTONPRESSED.setText(buttonCategory.toString());
        getLevelPressed();
        try {
            try {
                mainStage.setScene(new QuizGamePlay(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return difficulty level.
     */
    public static String getLevelPressed() {
        return BUTTONPRESSED.getText();
    }

}
