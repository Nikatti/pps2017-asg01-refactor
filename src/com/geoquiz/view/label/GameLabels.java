package com.geoquiz.view.label;

/**
 * The menu's labels who a player can see.
 */
public enum GameLabels {
    /**
     * Username label.
     */
    USERNAME,
    /**
     * Password label.
     */
    PASSWORD

}
