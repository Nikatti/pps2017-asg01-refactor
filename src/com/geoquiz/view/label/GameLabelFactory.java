package com.geoquiz.view.label;

import javafx.scene.paint.Color;

/**
 * static factory for labels inside menu.
 */
public final class GameLabelFactory {

    private GameLabelFactory() {
    }

    /**
     * 
     * @param name
     *            the text of label.
     * @param color
     *            the color of label.
     * @param font
     *            the text font.
     * @return the label.
     */
    public static GameLabel createMyLabel(final String name, final Color color, final double font) {
        return new GameLabelImpl(name, color, font);
    }

}
